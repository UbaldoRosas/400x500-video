var gulp = require('gulp')
var compass = require('gulp-compass')
var browserSync = require('browser-sync').create()
var zip = require('gulp-zip')

// Compass config
var config = {
  project: __dirname,
  style: 'compressed',
  sass: 'src/sass',
  image: 'src/images',
  css: 'dist/',
  generated_images_path: 'dist/img'
}

gulp.task('compass', function() {
  gulp.src('./src/sass/style.scss')
    .pipe(compass(config))
    .on('error', function(error) {
      // Would like to catch the error here
      console.log(error);
      this.emit('end');
    })
    .pipe(browserSync.stream())
})

// Compass config for expand
var configExpand = {
  project: __dirname,
  style: 'compressed',
  sass: 'src/sass',
  image: 'src/images',
  css: 'dist/panels/expand',
  generated_images_path: 'dist/panels/expand/img'
}

gulp.task('compass-expand', function() {
  gulp.src('./src/sass/expand.scss')
    .pipe(compass(configExpand))
    .on('error', function(error) {
      // Would like to catch the error here
      console.log(error);
      this.emit('end');
    })
    .pipe(browserSync.stream())
})

// Generate zip
gulp.task('build', function() {
  var dir = __dirname.split(/[\\\/]/)
  var zipName = dir[dir.length - 1] + '.zip'

  return gulp.src('./dist/**/*')
    .pipe(zip(zipName))
    .pipe(gulp.dest('./'))
})

// Static Server + watching scss/html files
gulp.task('serve', ['compass', 'compass-expand'], function() {
  browserSync.init({
    server: "./dist/"
  })

  gulp.watch("src/sass/*.scss", ['compass', 'compass-expand'])
  gulp.watch("dist/**/*.html").on('change', browserSync.reload)
  gulp.watch("dist/**/*.js").on('change', browserSync.reload)
})
